package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private static AlunoDAO alunoDAO = new AlunoDAO();
	private static DateHelper dateUtil = new DateHelper();
	private static AlunoBO alunoBO = new AlunoBO(alunoDAO, dateUtil);

	// @BeforeAll
	public static void setupAll() {
		alunoDAO = new AlunoDAO();
		dateUtil = new DateHelper();
		alunoBO = new AlunoBO(alunoDAO, dateUtil);
	}

	@BeforeEach
	public void setup() {
		alunoDAO.excluirTodos();
	}

	@AfterAll
	public static void tearDownAll() {
		alunoDAO.excluirTodos();
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste # | entrada | saida esperada 1 | aluno nascido em 2003 |
	 * * 16
	 */
	public void testarCalculoIdadeAluno1() {
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 10;

		// Dados de entrada / saída esperada
		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		// Executar o método que eu desejo testar
		alunoBO.atualizar(aluno1);

		// Obter a saída atual
		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(matricula);

		// comparação entre o aluno1 e o alunoAtual:
		Assertions.assertEquals(aluno1, alunoAtual);
	}

	// Atenção: NÃO É UM TESTE! É APENAS UM EXEMPLO DE ALTERAÇÃO NO BUILDER.
	@Test
	public void exemplificarAlteracoesNoBuilderSemMas() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comMatricula(45).comNome("José").nascidoEm(2000).ativo();
		Aluno aluno1 = alunoBuilder.comNome("Maria").build();
		Aluno aluno2 = alunoBuilder.comMatricula(60).build();
		Aluno aluno3 = alunoBuilder.cancelado().build();
		Aluno aluno4 = alunoBuilder.nascidoEm(1950).build();

		System.out.println("\n\nAlterações no builder SEM o uso do MAS:");
		System.out.println("aluno1=" + aluno1); // 45, Maria, 2000, ativo
		System.out.println("aluno2=" + aluno2); // 60, Maria, 2000, ativo
		System.out.println("aluno3=" + aluno3); // 60, Maria, 2000, cancelado
		System.out.println("aluno4=" + aluno4); // 60, Maria, 1950, cancelado

	}

	// Atenção: NÃO É UM TESTE! É APENAS UM EXEMPLO DE ALTERAÇÃO NO BUILDER.
	@Test
	public void exemplificarAlteracoesNoBuilderComMas() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comMatricula(45).comNome("José").nascidoEm(2000).ativo();
		Aluno aluno1 = alunoBuilder.mas().comNome("Maria").build();
		Aluno aluno2 = alunoBuilder.mas().comMatricula(60).build();
		Aluno aluno3 = alunoBuilder.mas().cancelado().build();
		Aluno aluno4 = alunoBuilder.mas().nascidoEm(1950).build();

		System.out.println("\n\nAlterações no builder COM o uso do MAS:");
		System.out.println("aluno1=" + aluno1); // 45, Maria, 2000, ativo
		System.out.println("aluno2=" + aluno2); // 60, José, 2000, ativo
		System.out.println("aluno3=" + aluno3); // 45, José, 2000, cancelado
		System.out.println("aluno4=" + aluno4); // 45, José, 1950, ativo

	}

}
